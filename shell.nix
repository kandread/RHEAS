{ pkgs ? import <nixpkgs> {} }:

let
  climateserv = pkgs.python3Packages.buildPythonPackage rec {
    pname = "climateserv";
    version = "0.0.18";
    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-mi0dAtKXRJNKz+XXt6PDETejBeHL3BXSjyrPspg7lMg";
    };
  };
  gsw = pkgs.python3Packages.buildPythonPackage rec {
    pname = "gsw";
    version = "3.4.0";
    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-1FyDWvDlOSNenPRq5Y2UR+IG0UWxPs/nRHrXuPvc+G0";
    };
    propagatedBuildInputs = with pkgs.python3Packages; [
      numpy
    ];
  };
  coards = pkgs.python3Packages.buildPythonPackage rec {
    pname = "coards";
    version = "1.0.5";
    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-+HC2FCiOf5PI9UXLIewn/Vq9DA4lqx9Gd1JKlbp7z9c";
    };
  };
  pydap = pkgs.python3Packages.buildPythonPackage rec {
    pname = "Pydap";
    version = "3.2.2";
    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-hjJmQuJPQhWVp0sPmYbadteTKyd3aPUB/iFNclkr3EA";
    };
    propagatedBuildInputs = [
      coards
      gsw
      pkgs.python3Packages.beautifulsoup4
      pkgs.python3Packages.numpy
      pkgs.python3Packages.webob
      pkgs.python3Packages.docopt
      pkgs.python3Packages.jinja2
      pkgs.python3Packages.requests
      pkgs.python3Packages.netcdf4
      pkgs.python3Packages.webtest
    ];
    doCheck = false;
  };
  rioxarray = pkgs.python3Packages.buildPythonPackage rec {
    pname = "rioxarray";
    version = "0.11.1";
    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-mTLbBJjRO9GiZDsOqmV9PRVYEYOWrw3R6oba5vkeD/Q=";
    };
    propagatedBuildInputs = [
      pkgs.python3Packages.xarray
      pkgs.python3Packages.pyproj
      pkgs.python3Packages.packaging
      pkgs.python3Packages.rasterio
    ];
    doCheck = false;
  };
  pypi-packages = with pkgs.python3Packages; [
    setuptools
    python
    numpy
    pandas
    gdal
    requests
    scipy
    lxml
    netcdf4
    h5py
    beautifulsoup4
    psycopg2
    numba
    dask
    rasterio
    xarray
  ];
  rheas = pkgs.python3Packages.buildPythonPackage rec {
    name = "rheas";
    src = ./.;
    doCheck = false;
    propagatedBuildInputs = [
      pypi-packages
      rioxarray
      pydap
      climateserv
    ];
  };
  vic = pkgs.stdenv.mkDerivation {
    name = "vic";
    # src = pkgs.fetchgit {
    #   url = "https://github.com/UW-Hydro/VIC.git";
    #   rev = "VIC.4.2.d";
    #   sha256 = "sha256-BjxZYe2uR2QrPfr3/N59Kv3vfgrxPgATByLOVbxb6Fk=";
    # };
    src = external/vic/src;
    nativeBuildInputs = [ pkgs.gcc pkgs.automake ];
    buildPhase = ''
            sed -i "s|\/bin\/bash|${pkgs.bash}\/bin\/bash|" Makefile
            make
          '';
    hardeningDisable = pkgs.lib.optionals (pkgs.stdenv.isAarch64 && pkgs.stdenv.isDarwin) [ "stackprotector" ];
    installPhase = ''
            mkdir -p $out/bin
            cp vicNl $out/bin
          '';
  };
  dssat = pkgs.stdenv.mkDerivation {
    name = "dssat";
    src = external/dssat;
    buildInputs= [ ];
    buildPhase = " ";
    installPhase = ''
      mkdir -p $out/bin
      chmod +x mdssat
      cp mdssat $out/bin
    '';
  };
in
pkgs.stdenv.mkDerivation {
  name = "rheas-env";
  buildInputs = [
    vic
    dssat
    rheas
    pkgs.glibcLocales       
    pkgs.gdal
    (pkgs.postgresql.withPackages (p: [ p.postgis ]))
  ];
  shellHook = ''
    export PGDATA="../db"
    export PGHOST="localhost"
    export SOCKET_DIRECTORIES="$PWD/sockets"
    export POSTGRES_DB="rheas"
    mkdir -p $SOCKET_DIRECTORIES
    if [ ! -d $PGDATA ]; then
        initdb
        echo "unix_socket_directories = '$SOCKET_DIRECTORIES'" >> $PGDATA/postgresql.conf
        pg_ctl -l $PGDATA/logfile start
        createdb $POSTGRES_DB
        echo "CREATE EXTENSION postgis; CREATE EXTENSION postgis_raster;" | psql -d $POSTGRES_DB
        echo "CREATE SCHEMA crops; CREATE SCHEMA dssat; CREATE SCHEMA vic;" | psql -d $POSTGRES_DB
        echo "ALTER DATABASE $POSTGRES_DB SET postgis.enable_outdb_rasters = true;" | psql -d $POSTGRES_DB
        echo "ALTER DATABASE $POSTGRES_DB SET postgis.gdal_enabled_drivers TO 'GTiff';" | psql -d $POSTGRES_DB
        gunzip < data/crops.sql.gz | psql -d $POSTGRES_DB
        gunzip < data/dssat.sql.gz | psql -d $POSTGRES_DB
        gunzip < data/soils.sql.gz | psql -d $POSTGRES_DB
        gunzip < data/vic.sql.gz | psql -d $POSTGRES_DB
    else
        pg_ctl -l $PGDATA/logfile start
    fi
    function end {
        echo "Shutting down the database..."
        rm -rf $SOCKET_DIRECTORIES
        pg_ctl stop
    }
    trap end EXIT
  '';
}
